package prova.marco.provaocr;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.icu.text.DateFormat;
import android.icu.text.SimpleDateFormat;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class MainActivity extends AppCompatActivity {

    Button btn;
    Integer TAKEFROMGALLERYCODE = 111;
    private Bitmap selectedImage;
    private ImageView img;
    private TextView txt;
    private String totale;
    private Date date;
    private String nome;
    private String indirizzo;
    private String dettagli;
    private ConcurrentHashMap<Integer,String> details;
    private List<SimpleDateFormat> dateFormats;


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn = findViewById(R.id.add);
        img = findViewById(R.id.imageView);
        txt = findViewById(R.id.txt);
        dateFormats = new ArrayList<SimpleDateFormat>() {
            {
                add(new SimpleDateFormat("dd-MM-yyyy"));
                add(new SimpleDateFormat("dd/MM/yyyy"));
                add(new SimpleDateFormat("yyyy-MM-dd"));
                add(new SimpleDateFormat("yyyy/MM/dd"));
                add(new SimpleDateFormat("MM-dd-yyyy"));
                add(new SimpleDateFormat("MM/dd/yyyy"));
                ;
            }
        };
    }

    public void add (View view){
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto , TAKEFROMGALLERYCODE);
    }

    public String getTotale(FirebaseVisionText firebaseVisionText){
        Integer top = 0;
        Integer bottomRight = 10000;
        totale = "";
        for (FirebaseVisionText.TextBlock block: firebaseVisionText.getTextBlocks()) {
            for (FirebaseVisionText.Line line: block.getLines()) {
                Rect lineFrame = line.getBoundingBox();
                for (FirebaseVisionText.Element element: line.getElements()) {
                    String elementText = element.getText();
                    if(elementText.equalsIgnoreCase("TOTALE")){
                        top = lineFrame.top;
                    }
                }
            }
        }
        for(FirebaseVisionText.TextBlock block: firebaseVisionText.getTextBlocks()){
            Rect blockFrame = block.getBoundingBox();
            for (FirebaseVisionText.Line line: block.getLines()) {
                Rect lineFrame = line.getBoundingBox();
                for (FirebaseVisionText.Element element: line.getElements()) {
                    String elementText = element.getText();
                    Rect elementFrame = element.getBoundingBox();
                    if(blockFrame.top > top -20 && blockFrame.top < top +20){

                        if(elementFrame.bottom < bottomRight && elementText.trim().matches(".*\\d+.*")){
                            totale = elementText;
                            bottomRight = lineFrame.bottom;
                        }
                    }
                }
            }
        }
        return totale;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private String getDate(FirebaseVisionText firebaseVisionText){
        String data = "";
        for(FirebaseVisionText.TextBlock block: firebaseVisionText.getTextBlocks()){
            for (FirebaseVisionText.Line line: block.getLines()) {
                for (FirebaseVisionText.Element element: line.getElements()) {
                    String elementText = element.getText();
                    for (SimpleDateFormat format : dateFormats) {
                        try {
                            format.setLenient(false);
                            date = format.parse(elementText);
                            data = format.format(date);
                        } catch (ParseException e) {

                        }
                    }
                }
            }
        }
        return data;
    }

    public String getName(FirebaseVisionText firebaseVisionText){
        Integer top = 10000;
        Integer firstBlock = 10000;
        nome = "";
        for (FirebaseVisionText.TextBlock block: firebaseVisionText.getTextBlocks()) {
            Rect blockFrame = block.getBoundingBox();
            for (FirebaseVisionText.Line line: block.getLines()) {
                for (FirebaseVisionText.Element element: line.getElements()) {
                    String elementText = element.getText();
                    if(elementText.equalsIgnoreCase("Via") || elementText.equalsIgnoreCase("Strada") || elementText.equalsIgnoreCase("Viale") || elementText.equalsIgnoreCase("Piazza") || elementText.equalsIgnoreCase("Corso")){
                        if(blockFrame.top < top){
                            top = blockFrame.top;
                        }
                    }
                    if (blockFrame.top < firstBlock){
                        firstBlock = blockFrame.top;
                    }
                }
            }
        }
        for(FirebaseVisionText.TextBlock block: firebaseVisionText.getTextBlocks()){
            for (FirebaseVisionText.Line line: block.getLines()) {
                Rect lineFrame = line.getBoundingBox();
                String lineText = line.getText();
                if ((lineFrame.top < top +30) && lineText != indirizzo){
                    nome = lineText;
                }

            }
        }

        if(nome.equals("")){
            for(FirebaseVisionText.TextBlock block: firebaseVisionText.getTextBlocks()){
                for (FirebaseVisionText.Line line: block.getLines()) {
                    Rect lineFrame = line.getBoundingBox();
                    String lineText = line.getText();
                    if ((lineFrame.top < firstBlock +30)){
                        nome = lineText;
                    }

                }
            }
        }

        return nome;
    }

    public String getAdress(FirebaseVisionText firebaseVisionText){
        Integer top = 10000;
        indirizzo = "";
        for (FirebaseVisionText.TextBlock block: firebaseVisionText.getTextBlocks()) {
            Rect blockFrame = block.getBoundingBox();
            for (FirebaseVisionText.Line line: block.getLines()) {
                String lineText = line.getText();
                for (FirebaseVisionText.Element element: line.getElements()) {
                    String elementText = element.getText();
                    if(elementText.equalsIgnoreCase("Via") || elementText.equalsIgnoreCase("Strada") || elementText.equalsIgnoreCase("Viale") || elementText.equalsIgnoreCase("Piazza") || elementText.equalsIgnoreCase("Corso")){
                        if(blockFrame.top < top){
                            top = blockFrame.top;
                            indirizzo = lineText;
                        }
                    }
                }
            }
        }
        return indirizzo;
    }

    public ConcurrentHashMap<Integer,String> getDetails(FirebaseVisionText firebaseVisionText){
        Integer topAddress = 10000;
        Integer bottomAddress = 10000;
        Integer topTotale = 10000;
        dettagli = "";
        details = new ConcurrentHashMap<Integer,String>();
        for (FirebaseVisionText.TextBlock block: firebaseVisionText.getTextBlocks()) {
            Rect blockFrame = block.getBoundingBox();
            for (FirebaseVisionText.Line line: block.getLines()) {
                for (FirebaseVisionText.Element element: line.getElements()) {
                    String elementText = element.getText();
                    if(elementText.equalsIgnoreCase("Via") || elementText.equalsIgnoreCase("Strada") || elementText.equalsIgnoreCase("Viale") || elementText.equalsIgnoreCase("Piazza") || elementText.equalsIgnoreCase("Corso")){
                        if(blockFrame.top < topAddress){
                            topAddress = blockFrame.top;
                            bottomAddress = blockFrame.bottom;
                        }
                    }
                    if(elementText.equalsIgnoreCase("TOTALE")){
                        topTotale = blockFrame.top;
                    }
                }
            }
        }
        for(FirebaseVisionText.TextBlock block: firebaseVisionText.getTextBlocks()){
            Rect blockFrame = block.getBoundingBox();
            for (FirebaseVisionText.Line line: block.getLines()) {
                Rect lineFrame = line.getBoundingBox();
                String lineText = line.getText();
                if (blockFrame.top > bottomAddress && blockFrame.bottom < topTotale) {
                    Integer currentLine = lineFrame.bottom;
                    if(!lineText.trim().matches(".*\\d+.*")){
                        details.put(currentLine,lineText);
                        Toast.makeText(this, "lineTesto: "+lineText, Toast.LENGTH_SHORT).show();
                    }
                    //if(details.size() == 0){
                       // details.put(currentLine,lineText);
                    //} else {
                        /*Set<Integer> keySet = details.keySet();
                        for(Integer key:keySet){
                            if (currentLine > key - 10 || currentLine < key + 10) {
                                String tmp = details.get(currentLine);
                                tmp += lineText;
                                details.remove(currentLine);
                                details.put(currentLine, tmp);
                            } else {
                                details.put(currentLine, lineText);
                            }*/
                        //}


                    //}
                }
            }

        }

        for(FirebaseVisionText.TextBlock block: firebaseVisionText.getTextBlocks()){
            Rect blockFrame = block.getBoundingBox();
            for (FirebaseVisionText.Line line: block.getLines()) {
                Rect lineFrame = line.getBoundingBox();
                String lineText = line.getText();
                if (blockFrame.top > bottomAddress && blockFrame.bottom < topTotale) {
                    Integer currentLine = lineFrame.bottom;
                    if(lineText.trim().matches(".*\\d+.*")) {
                        if (details.size() == 0) {
                            details.put(currentLine, lineText);
                            //Toast.makeText(this, lineText, Toast.LENGTH_SHORT).show();
                        } else {
                            for (Integer i : details.keySet()) {
                                if (currentLine > i - 5 || currentLine < i + 5) {
                                    String tmp = "";
                                    tmp = details.get(i);
                                    Toast.makeText(this, "current: "+details.get(i), Toast.LENGTH_SHORT).show();
                                    tmp += lineText;
                                    Toast.makeText(this, "tmp: "+tmp, Toast.LENGTH_SHORT).show();
                                    details.remove(currentLine);
                                    details.put(currentLine, tmp);
                                }
                            }
                        }
                    }
                }
            }

        }

        return details;
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        if (imageReturnedIntent != null) {
            try {
                final Uri imageUri = imageReturnedIntent.getData();
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                selectedImage = BitmapFactory.decodeStream(imageStream);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
            }
            FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(selectedImage);
            img.setImageBitmap(selectedImage);
            FirebaseVisionTextRecognizer detector = FirebaseVision.getInstance()
                    .getOnDeviceTextRecognizer();

            Task<FirebaseVisionText> result =
                    detector.processImage(image)
                            .addOnSuccessListener(new OnSuccessListener<FirebaseVisionText>() {
                                @RequiresApi(api = Build.VERSION_CODES.N)
                                @Override
                                public void onSuccess(FirebaseVisionText firebaseVisionText) {
                                    getDetails(firebaseVisionText);
                                   // Toast.makeText(MainActivity.this, "Totale: "+getTotale(firebaseVisionText), Toast.LENGTH_SHORT).show();
                                   // Toast.makeText(MainActivity.this, "Data: "+getDate(firebaseVisionText), Toast.LENGTH_SHORT).show();
                                   // Toast.makeText(MainActivity.this, "Nome: "+getName(firebaseVisionText), Toast.LENGTH_SHORT).show();
                                   // Toast.makeText(MainActivity.this, "Indirizzo: "+getAdress(firebaseVisionText), Toast.LENGTH_SHORT).show();
                                    for(Integer i : getDetails(firebaseVisionText).keySet()){
                                        //Toast.makeText(MainActivity.this, "Dettagli: "+getDetails(firebaseVisionText).get(i), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            })
                            .addOnFailureListener(
                                    new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            // Task failed with an exception
                                            // ...
                                        }
                                    });

        } else {
        }
    }


}
